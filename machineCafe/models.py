from django.db import models


class Boisson(models.Model):
    title = models.CharField(max_length=100)
    price = models.IntegerField(blank=False, default=1)
    time = models.IntegerField(blank=False, default=1)


# Create your models here.

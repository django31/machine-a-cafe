from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from machineCafe.serializer import UserSerializer, GroupSerializer, BoissonSerializer

from django.http import JsonResponse

from machineCafe.models import *
from django.views.decorators.csrf import csrf_exempt


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


@csrf_exempt
def get_data(request):
    data = Boisson.objects.all()
    if request.method == 'GET':
        serializer = BoissonSerializer(data, many=True)
        return JsonResponse(serializer.data, safe=False)

import { Component, OnInit } from '@angular/core';
import { BoissonSelectService} from "../boisson-select.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

  PossiblePrice = [
    0.20,0.50,1,2
  ];
  Cost: number;

  constructor(
    private boissonSelectService: BoissonSelectService,
    private router:Router
  ) { }

  ngOnInit() {
    this.Cost = 0;
    let span =document.getElementById('restToPaid')
      .innerHTML = this.boissonSelectService.select_boisson.price.toString() + "€";

  }

  AddPiece(value):void {
    this.Cost = this.Cost + value;
    let restTopaid = this.boissonSelectService.select_boisson.price - this.Cost;
    if (restTopaid > 0) {
      let span = document.getElementById('restToPaid');
      span.innerHTML = restTopaid.toString() + "€";
    }
    else {
      this.boissonSelectService.money_back = - restTopaid;
      this.router.navigate(['preparation'])
    }
  }



}

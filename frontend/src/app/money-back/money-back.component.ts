import { Component, OnInit } from '@angular/core';
import {BoissonSelectService} from "../boisson-select.service";

@Component({
  selector: 'app-money-back',
  templateUrl: './money-back.component.html',
  styleUrls: ['./money-back.component.css']
})
export class MoneyBackComponent implements OnInit {

  money_back=this.boissonSelectService.money_back;

  constructor(private boissonSelectService: BoissonSelectService) { }

  ngOnInit() {

  }


}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SelectBoissonComponent} from "./select-boisson/select-boisson.component";
import {PaymentComponent} from "./payment/payment.component";
import {PreparationComponent} from "./preparation/preparation.component";
import {MoneyBackComponent} from "./money-back/money-back.component";
import {ScreenComponent} from "./screen/screen.component";

const routes: Routes = [
  {path:'select',component:SelectBoissonComponent},
  {path:'payment',component:PaymentComponent},
  {path:'preparation',component:PreparationComponent},
  {path:'moneyback',component: MoneyBackComponent},
  {path:'', component:ScreenComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

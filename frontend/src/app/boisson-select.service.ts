import { Injectable } from '@angular/core';
import {Boisson} from "./boisson";

@Injectable({
  providedIn: 'root'
})

export class BoissonSelectService {

  select_boisson: Boisson;
  money_back: number=0;

  constructor() { }

}

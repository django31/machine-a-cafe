import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SelectBoissonComponent } from './select-boisson/select-boisson.component';
import { PaymentComponent } from './payment/payment.component';
import { PreparationComponent } from './preparation/preparation.component';
import {BoissonSelectService} from "./boisson-select.service";
import { MoneyBackComponent } from './money-back/money-back.component';
import { ScreenComponent } from './screen/screen.component';
import {HttpClient, HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent,
    SelectBoissonComponent,
    PaymentComponent,
    PreparationComponent,
    MoneyBackComponent,
    ScreenComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule
  ],
  providers: [BoissonSelectService],
  bootstrap: [AppComponent]
})
export class AppModule { }

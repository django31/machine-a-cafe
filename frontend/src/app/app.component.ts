import { Component } from '@angular/core';
import {BoissonSelectService} from "./boisson-select.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'La machine à café';
}



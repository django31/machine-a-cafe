import { TestBed } from '@angular/core/testing';

import { BoissonSelectService } from './boisson-select.service';

describe('BoissonSelectService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BoissonSelectService = TestBed.get(BoissonSelectService);
    expect(service).toBeTruthy();
  });
});

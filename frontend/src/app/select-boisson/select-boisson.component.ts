import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BOISSONS} from "../boissons";
import {BoissonSelectService} from "../boisson-select.service";
import {Boisson} from "../boisson";

@Component({
  selector: 'app-select-boisson',
  templateUrl: './select-boisson.component.html',
  styleUrls: ['./select-boisson.component.css']
})
export class SelectBoissonComponent implements OnInit {

  
  Boissons : [Boisson];
  private req: any;
  url : string = 'http://localhost:8000/getData/';

  constructor(
    private boissonSelectService: BoissonSelectService,
    private http: HttpClient,
  )
  { }

  ngOnInit() {
    console.log('initialisation with url' + this.url);
    this.req = this.http.get(this.url).subscribe(data =>{
      console.log(data);
      this.Boissons = data as [Boisson];
    })
  }
  
  ngOnDestroy(){
    this.req.unsubscribe()
  }

  SelectBoisson(boisson): void{
    this.boissonSelectService.select_boisson = boisson;
    console.log(boisson);
  }

}

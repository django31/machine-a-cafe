import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectBoissonComponent } from './select-boisson.component';

describe('SelectBoissonComponent', () => {
  let component: SelectBoissonComponent;
  let fixture: ComponentFixture<SelectBoissonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectBoissonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectBoissonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, OnInit} from '@angular/core';
import {BoissonSelectService} from "../boisson-select.service";
import * as d3 from 'node_modules/d3'
import {Router} from "@angular/router";

@Component({
  selector: 'app-preparation',
  templateUrl: './preparation.component.html',
  styleUrls: ['./preparation.component.css']
})
export class PreparationComponent implements OnInit {

  constructor(
    private router: Router,
    public boissonSelectService: BoissonSelectService) {
  }

  ngOnInit() {
    this.createCircleLoad();
    this.loadService();
  }

  loadService(): void {
    let timelimit = this.boissonSelectService.select_boisson.time;
    let speed = 50;
    let time = 0;
    let index = 0;
    let interval = setInterval(function () {
      index++;
      time += speed / 1000;
      if (time >= timelimit) {
        clearInterval(interval);
      }
      d3.select('#progressbar')
        .attr('width', function () {
          return (time / timelimit) * 300;
        });
      d3.select('#gCircleLoad')
        .attr('transform', "translate(250,50) rotate(" + (index % 100) * 3.6 + ")")
    }, speed);
  }

  createCircleLoad(): void {
    var rayon = 20;  // en pixel
    var circleNumber = [2, 2, 2, 2, 2, 2, 2, 2, 3, 2, 2, 2];
    d3.select("#circleLoad")
      .append("g")
      .attr('id', 'gCircleLoad')
      .selectAll(".circle")
      .data(circleNumber)
      .enter()
      .append('circle')
      .attr('class', 'circleload')
      .attr('r', function (d) {
        return d;
      })
      .attr('cx', function (d, i) {
        return rayon * Math.cos((360 / circleNumber.length) * (Math.PI / 180) * i);
      })
      .attr('cy', function (d, i) {
        return rayon * Math.sin((360 / circleNumber.length) * (Math.PI / 180) * i);
      })
  }

  checkProgressBar(): number {
   return Number(document.getElementById('progressbar').getAttribute('width'));
  }
}
